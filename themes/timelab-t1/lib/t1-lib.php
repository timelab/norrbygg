<?php


add_action( 'after_setup_theme', 'baw_theme_setup' );
function baw_theme_setup() {
    add_image_size( 'reference-thumb', 160, 118, array('center','center') );
    add_image_size( 'reference-list', 720, 530, array('center','center') );
    add_image_size( 'reference-full', 0, 750,false);
}

function cateringDisplay($atts){
	extract(shortcode_atts(array(
		"typ" => 'typ',
		"rubrik" => 'Rubrik'
	), $atts));

	$html = "";

	$podparams = array('where' => 'typ.meta_value="'.$typ.'"', 'orderby' => 'pris.meta_value ASC', 'limit' => 999);
	$objectList = pods('catering', $podparams);

	if ($objectList->total() > 0){
		$html = "<div class='cateringHeader'><h4>" . $rubrik . "</h4></div>";
		while ($objectList->fetch())
		{
			$html.= "<div class='row cateringItem'>\n";
				$html .= "<div class='menurowpadd'>\n";
				$html .= "<div class='col-xs-12 col-sm-10 cateringTitle'>". $objectList->field('post_title') . "</div>";
				$html .= "<div class='col-xs-12 col-sm-2 cateringPrice'>" .$objectList->field('pris') . ":-</div>";
				if($objectList->field('post_content')!=''){
					$html .= "<div class='col-xs-12 col-sm-10 cateringDescription'>" .strip_tags($objectList->display('post_content'),'<br><br/>') . "</div>";
				}
				$html .= "</div>\n";
			$html .= "</div>\n";
		}
	}


	return $html;
}
add_shortcode( 'catering', 'cateringDisplay' );


function emitBrands($cms)
{
	global $t1config;
	$html = "";
	
	if ($t1config['brands_enabled'] === false)
	{
		return $html;
	}
	
	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 6);
	$objectList = pods('brands', $podparams);
	if ($objectList->total() > 0)
	{
		$html .= "<div class='container showcases brands PodsPuffar {$t1config['hide_brands']}'>";
			$html .= "<div class='row'>";

			$classes = "";
			switch ($objectList->total())
			{
				case 1:
				case 2:
				case 3:
					$classes = "col-xs-4 col-sm-4 col-md-4";
					break;
				case 4:
					$classes = "col-xs-6 col-sm-3 col-md-3";
					break;
				case 5:
				case 6:
					$classes = "col-xs-6 col-sm-2 col-md-2";
					break;
				default:
					$classes = "col-xs-6 col-sm-2 col-md-2";
			}
			$border = "border";

			$i = 0;
			while ($objectList->fetch())
			{
				$i++;
				if ($i === $objectList->total()) { $border = ""; }

				$linkArr = $objectList->field('puff_lank');

				$html .= ($linkArr !== "") ? "<a href='{$linkArr}' target='_blank'>" : "";
				$html .= "<span class='{$classes} {$border} brand'>";
					$html .= "<span class='brand-bg'>";
						$html .= '<img src="' . $objectList->display('puff_bild') . '"/>';
						
						if (!empty($objectList->display('puff_bild_hover')))
						{
							$html .= '<img class="hover" src="' . $objectList->display('puff_bild_hover') . '"/>';
						}
					$html .= '</span>';
				$html .= '</span>';
				$html .= ($linkArr !== "") ? '</a>' : '';
			}
			$html .= "</div>";
		$html .= "</div>";
	}

	return $html;
}


function emitShowcases($cms)
{
	$html = "";

	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);
	if ($objectList->total() > 0)
	{
		$count = 0;

        $html .= "<div class='container showcases PodsPuffar'>";
        $html .= "<div class='row showcase-row'>";
        while ($objectList->fetch())
        {
            $count++;
            $linkArr = $objectList->field('puff_lank');
            $linkTarget = '_self';
            if(!empty($linkArr)){
                $podpermalink = get_permalink($linkArr['ID']);
            }else{
                $podpermalink = $objectList->field('extern_lank');
                $linkTarget = "_blank";
            }

            $html .= "<a href='$podpermalink' target='$linkTarget'>";
            $html .= "<span class='col-md-3 col-sm-6 col-xs-6 showcase pos$count'>";
            $html .= "<span class='showcase-bg'>";
            $html .= "<img src='{$objectList->display('puff_bild')}' alt='{$objectList->field('puff_text')}'/>";
            if (!empty($objectList->display('puff_bild_hover')))
            {
                $html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
            }
            $html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
            if ($objectList->field('puff_text') !== "")
            {
                $html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
            }
            $html .= "</span>";
            $html . "</span>";
            $html .= "</a>";
        }
        $html .= "</div>";
        $html .= "</div>";
	}

	return $html;
}

function emitShowcases_vertical($cms)
{
	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);

    $html = '<div class="showcases showcases-vertical">';
    if ($objectList->total() > 0)
    {
		// loop through items using pods::fetch
		$count = 0;
		while ($objectList->fetch())
        {
			$count++;
			$linkArr = $objectList->field('puff_lank');
			$podpermalink = get_permalink($linkArr['ID']);

			$html .= "<div class='col-xs-6 col-sm-6 col-md-12 showcase showcase-vertical pos$count'>";
				$html .= "<a href='$podpermalink' class='showcase-href'>";
					$html .= '<span class="showcase-bg">';
						$html .= '<img src="'.$objectList->display('puff_bild').'" alt="'.$objectList->field('puff_text').'" />';
						if (!empty($objectList->display('puff_bild_hover')))
						{
							$html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
						}
						$html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
						if ($objectList->field('puff_text') !== "")
						{
							$html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
						}
					$html .= '</span>';
				$html .='</a>';
			$html .= '</div>';
		}
	}
    $html .='</div>';
    return $html;
}


function emitSlider($cms, $sliderID)
{
	$html = "<div class='sliderShadow'></div>";
	$html .= "<div class='flexslider'>";
		$html .= "<ul class='slides'>";

        /** @var $slider Timelab\Cms\Objects\SliderObject */
        $slider = $cms->getApi('Slider')->getSlider($sliderID);

		foreach ($slider->getImages(true) as $slide)
		{
            $title      = $slide->getTitle();
            $subtitle   = $slide->getSubtitle();
            $text       = $slide->getText();

            $slideHtml = "";

            if ( !(empty($title) && empty($subtitle) && empty($text))  )
            {
                $slideHtml .= "<div class='slideText'>";

                    $href       = $slide->getHref();

                    $slideHtml .= empty($title) ? "" : "<h2>{$title}</h2>";
                    $slideHtml .= empty($subtitle) ? "" : "<h3>{$subtitle}</h3>";
                    $slideHtml .= empty($text) ? "" : "<p>{$text}</p>";
                $slideHtml .= "</div>";
            }


            $slideHtml .= "<img src='{$slide->getSrc()}'/>";

			$html .=  "<li>";
				$html .= empty($href) ? $slideHtml : "<a href='{$href}' target='{$slide->getTarget()}'>$slideHtml</a>";
			$html .= "</li>";
		}
		$html .= "</ul>";
	$html .= "</div>";
	
	return $html;
}


function emitSocial($cms)
{
	global $t1config;
	$html = "";
	
	if ($t1config['social_integration_enabled'] === false)
	{
		return $html;
	}
	
	$html .= "<div>";

    $html .= "<a href='{$t1config['href_facebook']}'target='_blank'>";
    $html .= "<img src='" . get_template_directory_uri() . "/assets/img/top_facebook.png' alt='Facebook' class='facebookicon'/>";
    $html .= "</a>";

    if (!empty($t1config['href_twitter']))
    {
        $html .= "<a href='{$t1config['href_twitter']}' target='_blank'>";
        $html .= "<img src='" . get_template_directory_uri() . "/assets/img/twitter.png' alt='Twitter'/>";
        $html .= "</a>";
    }


    $html .= "<a href='#' class='{$t1config['facebook_icon']} instagramBtn'>";
    $html .= "<img src='" . get_template_directory_uri() . "/assets/img/top_instagram.png' alt='Instagram' class='instagramicon'/>";
    $html .= "</a>";

	$html .= "</div>";
	
	return $html;
}


function getContactInfo($cms)
{
	// Loop through facilities to gather data.
	$facilities = array();
	foreach ($cms->getApi('Contact')->getFacilities() as $facility)
	{	
		$address = $facility->getAddress();
	
		$data = array();
		$data['address'] = $address->getStreet() . ' ' . $address->getZip() . ' ' . $address->getCity();
		$data['address_2'] = $address->getStreet() . ' ' . $address->getCity();
		foreach ($facility->getContactDetails() as $fUppgift)
		{
			if ($fUppgift->getValue() !== '')
			{
				$data[$fUppgift->getType()] = $fUppgift->getValue();
			}
		}
		$facilities[] = $data;
	}
	
	return $facilities;
}

//Walher for subpage menus
class My_Sub_Walker extends Walker
{
    public function walk( $elements, $max_depth)
    {
        global $isParent;
        $list = array ();
        $parent = $isParent;


        foreach ( $elements as $item )
        {
            if($parent == 0){
                //Sök topförälder
                foreach($item->classes as $class){
                    if($class == 'current-menu-item' || $class == 'current-menu-parent'){
                        $parent = $item->ID;
                    }
                }
            } else {
                if($item->menu_item_parent == $parent){
                    $list[] = "<li><a href='$item->url' class='" . str_replace('current-menu-item','active',implode(' ',$item->classes)) . "'>$item->title</a></li>";
                } else {

                }
            }
        }
        return join( "\n", $list );
    }
}


class JC_Walker_Nav_Menu extends Walker_Nav_Menu {

    var $menu_id = 0;
    var $menu_items = array();

    function __construct($id = 0){
        $this->menu_id = $id;
        $this->menu_items[] = $id;
    }


    function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
        if( !empty($this->menu_items) && !in_array($item->ID, $this->menu_items) && !in_array($item->menu_item_parent, $this->menu_items)){
            return false;
        }

        if(!in_array($item->ID, $this->menu_items)){
            $this->menu_items[] = $item->ID;
        }

        //On submeny get title from page instead of menu
        $page = get_page_by_path(str_replace(get_site_url(),'',$item->url));
        $item->title = $page->post_title;


        parent::start_el($output, $item, $depth, $args);
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if( !empty($this->menu_items) && !in_array($item->ID, $this->menu_items) && !in_array($item->menu_item_parent, $this->menu_items)){
            return false;
        }
        parent::end_el($output, $item, $depth, $args);
    }

}