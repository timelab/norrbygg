 $(function(){
     $('.wpcf7-form .LeadText').each(function () {
        var label = $('label', this), input = $('input,textarea', this);
        if (input.length > 0 && input.val().length) label.hide();
        label.click(function () {
            input.focus();
        });
        input.focus(function () {
            label.hide();
        }).blur(function () {
            if (!input.val().length) label.show();
        });
     });
 });
 


