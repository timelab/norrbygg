$(function () {

$(".checkboxText").click(function (){
    //denna funktion är för när man bara vill ha ETT album som kan vara aktivt
    $(".checkboxbtn").prop("checked", false);
    $(".checkboxText").removeClass("AlbumActive");
    $(this).addClass("AlbumActive");
    $(this).parent().find(".checkboxbtn").prop("checked", true);
    $(".thumbImgDiv").removeClass("HiddenDiv");
    $(".albumListan").addClass("HiddenDiv");
    $("#albumHeadline").text($(this).text());
    
});

//pita - kollar bilder i album och checkar i rätt box
$(".albumListan>.thumbDiv").click(function (){
    $('#cb'+$(this).attr('id').replace('td_','')).click();
    $('#cb'+$(this).attr('id').replace('td_','')).parent().find(".checkboxText").click();
});

$(".cb-button, .checkboxText").click(function (){
        
	$(".thumbDiv").each(function () {
		$thisThumb = $(this);
		$thisImg = $("img", this);
		   
		if ($(!$(this).hasClass("thumbHidden"))) {$thisImg.addClass("thumbFade")}
	  // console.log("startar bygge av formel");
		if (<?php 
					echo '((';
				   for($i = 0, $l = count($album); $i < $l; ++$i) {
								if ($i>0) {echo ' || ';};
							   echo ' ($thisThumb.hasClass("thumb'.$album[$i].'") && $("#cb'.$album[$i].'").prop("checked"))'; 
				   };
				   echo ') || (';   
				   //denna for är till för att ta hand om fallet då INGET album är valt
				   for($i = 0, $l = count($album); $i < $l; ++$i) {
								if ($i>0) {echo ' && ';};
							   echo ' (!$("#cb'.$album[$i].'").prop("checked"))'; 
				   }
				   
					echo '))';
				  
					
			 ?>){
				fadeIn($thisThumb, $thisImg);
			}else{
				fadeOut($thisThumb, $thisImg);
			}
							 
	  
	 });
});
 
 
function fadeIn($selectedDiv, $selectedImg){
    setTimeout(function(){ $selectedDiv.removeClass("thumbHidden");}, 500);
    setTimeout(function(){ $selectedImg.removeClass("thumbFade")}, 550);
}

function fadeOut($selectedDiv, $selectedImg){
    setTimeout(function(){$selectedDiv.addClass("thumbHidden");}, 500);
}


$(".thumbImg").click(function (){
    // öppna den stora bilden
$thumb=$(this).attr("src");

    $("#blurr").css("opacity","0.90").css("height",$("html").height()).css("min-height",$(window).height());
    $(".imgBtnLeft").css("opacity","0.6").css("height","100%").css("width","80px");
    $(".imgBtnRight").css("opacity","0.6").css("height","100%").css("width","80px");
    
   
        
        
   $(".thumbDiv").removeClass("Active");
   $(".thumbDiv").removeClass("Prev");
   $(".thumbDiv").removeClass("Next");
   
   $(this).parent().addClass("Active");
   nextImg($('.thumbDiv.Active'));
   prevImg($('.thumbDiv.Active'));
    
  
       $("#showImg>img").css("opacity","0");
        setTimeout(  function()   {  
               $("#showImg>img").attr("src", "" );

               setTimeout(  function()   {
                   zoomIn();
                    
               }, 200);

        }, 100);
 
});

$(".imgBtnLeft").click(function (e){
  // console.log("prev");
   $("#showImg>img").attr("src", $(".thumbDiv.Prev>img").attr("src").replace("-150x150","") );
   
   $(".thumbDiv").removeClass("Next");
   $(".thumbDiv.Active").addClass("Next");
   $(".thumbDiv").removeClass("Active");
   $(".thumbDiv.Prev").addClass("Active");
   $(".thumbDiv").removeClass("Prev");
   prevImg($('.thumbDiv.Active')); 
   $(".imgText").text($(".thumbDiv.Active>.pictureText").text());
   if($(".thumbDiv").hasClass("Prev")){$("#showImg>.imgBtnLeft").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
   if($(".thumbDiv").hasClass("Next")){$("#showImg>.imgBtnRight").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
rescale();
   e.preventDefault();
   e.stopPropagation();
})

$(".imgBtnRight").click(function (e){
   //console.log("next");
   $("#showImg>img").css("opacity","0");
   $("#showImg>img").attr("src", $(".thumbDiv.Next>img").attr("src").replace("-150x150","") );
   
   $(".thumbDiv").removeClass("Prev");
   $(".thumbDiv.Active").addClass("Prev");
   $(".thumbDiv").removeClass("Active");
   $(".thumbDiv.Next").addClass("Active");
   $(".thumbDiv").removeClass("Next");
   nextImg($('.thumbDiv.Active'));
   $(".imgText").text($(".thumbDiv.Active>.pictureText").text());
   if($(".thumbDiv").hasClass("Prev")){$("#showImg>.imgBtnLeft").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
   if($(".thumbDiv").hasClass("Next")){$("#showImg>.imgBtnRight").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnRight").css("opacity","0").css("width","0px");}

 rescale();
   e.preventDefault();
   e.stopPropagation();
})

function prevImg(current){
        if(current.prev().length > 0){
            if(current.prev().hasClass('thumbHidden')){
                prevImg(current.prev());
            }else {
                
                current.prev().addClass('Prev');
            }
        }
}

function nextImg(current){
        if(current.next().length > 0){
            if(current.next().hasClass('thumbHidden')){
                nextImg(current.next());
            }else {
                current.next().addClass('Next');
            }
        }
        

}


$("#showImg, #blurr").click(function (){
    
  //stänger ned bilden
    $(".thumbDiv").removeClass("Active");
    $(".thumbDiv").removeClass("Next");
    $(".thumbDiv").removeClass("Prev");
    
    $("#blurr").css("opacity","0").css("height","0%").css("min-height","0px");
  
    $("#showImg").css("margin-top","-2000px").css("opacity","0"); 
    
   
      $("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");
     $("#showImg>.imgBtnRight").css("opacity","0").css("width","0px");
     $("#showImg>.imgText").css("opacity","0");
   
    setTimeout(  function()   {  $("#showImg>img").attr("src", "" );}, 100);
});


    
function zoomIn()
{
     $("#showImg img").css("height","");
     $("#showImg img").css("width","");
   //console.log("height och width borta");
     // klick på thumbnail visar bilden
     $("#showImg").css("margin-top",$(document).scrollTop()-20).css("opacity","1");
     $("#showImg>img").attr("src", $thumb.replace("-150x150","") );
     
     $("#showImg>img").css("margin-top","0px").css("opacity","1");;
   
          
     $("#showImg>.imgBtnLeft").css("top","0px").css("left","0px").css("opacity","0.6");
     $("#showImg>.imgBtnRight").css("top","0px").css("right","0px").css("opacity","0.6");
     $("#showImg>.imgText").css("bottom","0px").css("opacity","1");
     
     $(".imgText").text($(".thumbDiv.Active>.pictureText").text());
     if($(".thumbDiv").hasClass("Prev")){$("#showImg>.imgBtnLeft").css("opacity","0.5").css("width","80px");}else{ $("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
     if($(".thumbDiv").hasClass("Next")){$("#showImg>.imgBtnRight").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnRight").css("opacity","0").css("width","0px");}

     rescale();
        



     
}

function rescale()
{
    $("#showImg img").css("height","");
    $("#showImg img").css("width","");
    $("#showImg img").css("opacity","0");
    setTimeout(function()
    {
   
            $("#showImg").width(img_width);
            
            // här räknar vi ut bildens storlek när den ska visas
            var window_width=$(".container").width()-60;
            var window_height=$(window).height()-250;
            var img_width=$("#showImg img").width();
            var img_height=$("#showImg img").height();
            var img_ratio=0;
            
         //   console.log("IMG width:" + img_width + " / height:" + img_height);
         //   console.log("WIN width:" + window_width + " / height:" +window_height);
            
            img_ratio=window_width/img_width; // ratio för att procentuellt minska bilden i sidled
          //  console.log("asd");
          //  console.log("ratio:" +img_ratio); 
            // om bilden är bredaren än fönster, så skalar vi ned höjd + bredd
            if (img_width>window_width){
           //         console.log("bild för bred");
                    img_height=img_height*img_ratio;
                    img_width=window_width;
                    img_height=img_height.toFixed(0);
            }
            img_ratio=window_height/img_height; // ratio för att procentuellt minska bilden i höjdled
             //om bilden är högre än fönstret, så skalar vi ned höjd+bredd
            if (img_height>window_height){
                
                 console.log("bild för hög");
                 img_width=img_width*img_ratio;
                 img_height=window_height;
                 img_width=img_width.toFixed(0);
            }
            
          //  console.log("IMG width:" + img_width + " / height:" + img_height);
            
            $("#showImg img").height(img_height).width(img_width);
            $("#showImg").css("left","50%");
            $("#showImg").css("margin-left",(-1)*img_width/2);
            
            $("#showImg").width(img_width);
  
            
            
            var window_width=0;
            var window_height=0;
            var img_width=0;
            var img_height=0;
            var img_ratio=0;
            $("#showImg img").css("opacity","1");
        ;}, 200);
 
    
    
}
});