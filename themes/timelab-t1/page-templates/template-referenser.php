<?php
/*
Template Name: Referenser Mall
*/

$referencestype='nyproduktion-och-storre-renoveringar';
$pageID;
if ( have_posts() ) {
    while ( have_posts() ) {
        $pageID = get_the_ID();
        $referencestype =  get_post_meta( $pageID, 'referenstyp', true );
        break;
    }
}
?>

<div class="wrap container mainText PodsBrands subpage" role="document">
    <div class="row relative">
        <div class="col-xs-12 subHeader">
            <div class="page-header">
                <h1>Referenser</h1>
            </div>
        </div>
    </div>

    <div class="row relative minPageHeight">
        <div class="col-xs-12 subContent">
            <?php get_template_part('templates/content', 'page'); ?>
        </div>
        <div class="col-sx-12 subContent">
            <a href="referenser-nyproduktion-och-storre-renoveringar" class="tag <?php echo $referencestype == 'nyproduktion-och-storre-renoveringar' ? 'active' : ''?>">Nyproduktion och större renoveringar</a>
            <a href="referenser-badrum" class="tag <?php echo $referencestype == 'badrum' ? 'active' : ''?>">Badrum</a>
            <a href="referenser-kok" class="tag <?php echo $referencestype == 'kok' ? 'active' : ''?>">Kök</a>
            <a href="referenser-ovrigt" class="tag <?php echo $referencestype == 'ovrigt' ? 'active' : ''?>">Övrigt</a>

            <div class="row">
                <?php


                $podparams = array('where' => 'visa_pa_sidan.meta_value=True and referensstatus.meta_value = "' . $referencestype . '"', 'orderby' => 'ordning.meta_value ASC', 'limit' => 999);
                $objectList = pods('referensjobb', $podparams);

                $html;
                if ($objectList->total() > 0) {
                    while ($objectList->fetch())
                    {
                        $images = $objectList->field('bilder');
                        $html .= '<div class="col-sm-6 col-md-4 reference-item">';
                            $html .= '<img src="' . pods_image_url($images[0]['ID'],'reference-list') . '"/>';
                            $html .= '<div class="info-box">';
                                $html .= '<h4>' . $objectList->field('post_title') . '</h4>';
                                $html .= substr($objectList->field('content'),0,100);
                            $html .= '</div>';
                            $html .= '<div class="button-box padded"><a href="' . $objectList->field('permalink') . '" class="gradient">Klicka här för mer bilder</a></div>';
                        $html .= '</div>';

                    }
                }else{
                    $html = '<div class="col-xs-12">Vi har tyvärr inte lagt upp något under denna kategori än</div>';
                }
                echo $html;

                ?>


            </div>

        </div>
    </div>
</div>
