<?php
/*
Template Name: Hittalunchen Mall
*/

$site_url = get_site_url();

$curl = curl_init();
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_URL, "http://www.hittalunchen.se/access/AllstarJSON2.aspx?companyId=" . get_post_meta(get_the_ID(), 'hittalunchen_companyId', true));

$result = curl_exec($curl);

$meny = json_decode($result);


?>

<div class="wrap container mainText PodsBrands subpage" role="document">
	<div class="row relative">
	    <div class="col-xs-12 subHeader">
			<div class="page-header">
				<h1>Lunchmeny v. <?php echo $meny->week; ?></h1>
			</div>
	    </div>
    </div>
    
    <div class="row relative minPageHeight">
	   	<div class="col-md-8 subContent menu">
			<?php
				//Loopa ut luncherna här
				$lunch = "";

				$lunch .= "<div class='row'><div class='col-sm-12 stdtillbehor'>" . nl2br($meny->meny[0]->Company->stdTillbehor) . "</div></div>\n";

				foreach($meny->meny[0]->Days as $day) {
					$lunch .= "<div class='row'>\n";
					$lunch .= "<div class=\"col-sm-12  menuday\"><h4>".$day->DayName."</h4></div>";
					$lunch .= "<div class='col-sm-12'>\n";
					foreach($day->Menus as $Menu){
						/*
						$lunch .= "<div class=\"menu-name span6 offset1\">".$Menu->Title."</div>";
						echo "<div class=\"camera-placeholder\"></div>";
						echo "<div class=\"menu-price food span1\">".$Menu->Price.":-</div>";
						echo "<div class=\"description-holder span5 offset1\">".$Menu->Description."</div>";
						*/
						$lunch .= "<div class='menurowpadd'><div class='row menurow'>\n";
						$lunch .= "<div class='col-sm-10'><strong>" . $Menu->Title . "</strong><br /><i>" . $Menu->Description . "</i></div>\n";
						$price = $Menu->Price !='' ? $Menu->Price.':-' : '';
						$lunch .= "<div class='col-sm-2 menuprice'><strong>" . $price . "</strong></div>\n";
						$lunch .= "</div></div>\n";
					}
					$lunch .= '</div>';
					$lunch .= '</div>';
				}





				echo $lunch;
			?>
	   	</div>
	        
       	<div class="col-md-4 subBorderLeft">
           	<?php echo emitShowcases_vertical($cms); //exists in t1-lib.php ?>
    	</div>
	</div>
</div>
