<?php
/*
Template Name: Undersida Mall
*/

global $haveParent;
global $isParent;

?>

<div class="wrap container mainText PodsBrands subpage" role="document">
	<div class="row relative">
	    <div class="col-xs-12 subHeader">
	    	<?php get_template_part('templates/page', 'header'); ?>
	    </div>
    </div>
    
    <div class="row relative minPageHeight">
	   	<div class="col-md-8 subContent">
			<?php get_template_part('templates/content', 'page'); ?>
	   	</div>
	        
       	<div class="col-md-4 subBorderLeft">
       		<?php if ($haveParent || $isParent > 0){ ?>
                <div class="boxrelative">
                    <div class="contentboxtop"></div>
                    <div class="contentbox submenucontainer">
                        <?php
                        global $topParent;
                        wp_nav_menu( array('menu' => 'primary_navigation', 'menu_class' => 'submenu', 'depth' => 4, 'walker' => new JC_Walker_Nav_Menu($topParent)) );
                        ?>
                    </div>
                </div>
            <?php } ?>
           	<?php echo emitShowcases_vertical($cms); //exists in t1-lib.php ?>
    	</div>
	</div>
</div>
