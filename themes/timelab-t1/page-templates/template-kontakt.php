<?php
/**
 *	Template Name: Kontaktsida Mall
 */
$site_url = get_site_url();
?>







<?php
global $cms;
$tabs = '';

$panels = '';
$active = 'active';
$facilities = $cms->getApi('Contact')->getFacilities();
$jscript = '';
$repArr = array("(0)", "-", " "); //array för att fixa till telefonnr i länkar.

/** @var $facility Timelab\Cms\Objects\Facility */
foreach ($facilities as $facility)
{
	$address = $facility->getAddress();
	$openinghours = $facility->getOpeningHours();
	$contactDetails = $facility->getContactDetails();

	$tabs .= "<li class='" . str_replace('in ', '', $active) . "'>";
	$tabs .= "<a href='#{$facility->getSlug()}' data-toggle='tab' data-lat='{$address->getLat()}' data-long='{$address->getLong()}'>";
	$tabs .= $facility->getTitle();
	$tabs .= "</a>";
	$tabs .= "</li>";

	//Personal & Karta
	$mapstaff = '<div id="map-canvas" style="height: 310px"></div>';
	$mapstaff .= '<div class="mapBottom"></div>';

	ob_start();
	get_template_part('templates/content', 'page');
	$mapstaff .= ob_get_contents();
	ob_end_clean();

	$mapstaff .='<div class="staff-list">';
	foreach ($facility->getPersonnel() as $person)
	{
		$mapstaff .= '<div class="col-xs-12 col-md-4 col-sm-6 personal">';
		if ($person->getImage() !== null)
		{
			$mapstaff .= "<img src='" . $person->getImage()->getSrc() . "' alt='{$person->getName()}' />";
		}

		$mapstaff .= "<div class='PL_infosquare'>";
		$mapstaff .= "<div class='PL_name'>{$person->getName()}</div>";
		$mapstaff .= "<div class='PL_role'>{$person->getDescription()}</div>";

		$pUppgifter = $person->getContactDetails();
		foreach ($pUppgifter as $pUppgift)
		{
			if ($pUppgift->getValue() !== '')
			{
				$mapstaff .= "<div class='PL_Uppgift PL_{$pUppgift->getType()}'>";
				if ($pUppgift->getType() === 'email')
				{
					$mapstaff .= "<a href='mailto:{$pUppgift->getValue()}'>[E-post]</a>";
				}
				else
				{
					$mapstaff .= "<span class='key'>Tel: </span>";
					$mapstaff .= $pUppgift->getFormatted();
				}
				$mapstaff .= "</div>";
			}
		}
		$mapstaff .= "</div><!-- /infosquare -->";
		$mapstaff .= "</div><!-- /personal -->";
	}
	$mapstaff .= "</div>";


	//Kontaktinformation
	$contact ='<div class="row contact-column">';
	$contact .= '<div class="col-xs-12 visible-xs col-sm-12 visible-sm hidden-md hidden-lg">';
	$contact .= '<hr>';
	$contact .= '</div>';
	$contact .='<div class="col-xs-12 col-sm-12 col-md-12 contactInfo">';
	$contact .= "<h3>{$facility->getTitle()}</h3>\n";

	//Företagsuppgifter
	$contact .= "<span class='contactHeader'>Besöksadress Luleå</span>\n";
	$contact .= "<span class='contactText'>";
	$contact .= "Nordkalottvägen 10";
	$contact .= "<span class='hidden-my-sm'>,</span>";
	$contact .= "<br class='visible-my-sm'/>972 52 Luleå";
	$contact .= "</span>\n";

	$contact .= "<span class='contactHeader'>Besökadress Piteå</span>\n";
	$contact .= "<span class='contactText'>";
	$contact .= "{$address->getStreet()}";
	$contact .= "<span class='hidden-my-sm'>,</span>";
	$contact .= "<br class='visible-my-sm'/>{$address->getZip()} {$address->getCity()}";
	$contact .= "</span>\n";
	
	$contact .= "<span class='contactHeader'>Postadress</span>\n";
	$contact .= "<span class='contactText'>";
	$contact .= "{$address->getStreet()}";
	$contact .= "<span class='hidden-my-sm'>,</span>";
	$contact .= "<br class='visible-my-sm'/>{$address->getZip()} {$address->getCity()}";
	$contact .= "</span>\n";





	//Telefonnr, email
	foreach($contactDetails as $fUppgift)
	{
		if ($fUppgift->getValue() != '')
		{
			$contact .= ($fUppgift->getType() == 'email') ? "<span class='contactHeader'>Epost: </span>\n<span class='contactText'><a href='mailto:".$fUppgift->getValue()."'>".$fUppgift->getValue()."</a></span>\n" : "<span class='contactHeader'>Telefon Piteå: </span>\n<span class='contactText'><a href='tel:".str_replace($repArr,'',$fUppgift->getValue())."'> ".$fUppgift->getValue()."</a></span><span class='contactHeader'>Telefon Luleå: </span>\n<span class='contactText'><a href='tel:0920224414'>0920-22 44 14</a></span>\n";
		}
	}
	$contact .="</div>";

	//Öppettider
	if (count($openinghours) > 0)
	{
		$contact .= '<div class="col-xs-12 col-sm-12 col-md-12">';
		$contact .= '<hr>';
		$contact .= '</div>';

		$contact .='<div class="col-xs-12 col-sm-6 col-md-12 showcaseX contactInfo">';
		$contact .= "<h3>Öppettider</h3>\n";
		$contact .= "<div class='oppettiderfix'>\n";
		foreach($openinghours as $tid)
		{
			$to = $tid->getTo();
			$mall = (!empty($to)) ? "<div class='col-xs-6 contactText'>%s:</div><div class='col-xs-6 contactText'>%s - %s</div>" : "<div class='col-xs-6 contactText'>%s:</div><div class='col-xs-6 contactText'>%s</div>";
			$contact .= sprintf($mall,$tid->getTitle(),$tid->getFrom(),$tid->getTo());
			$contact .= "\n";
		}
		$contact .= "</div>";
		$contact .= "</div>\n";
	}
	$contact .= "<div class='clear'></div>";

	$contact .= '<div class="col-xs-12 col-sm-12 col-md-12">';
	$contact .= '<hr>';
	$contact .= '</div>';

	$contact .= '<div class="col-xs-12 col-sm-12 col-md-12">';
	$contact .= '<h3>Mail via formulär</h3>';
	$contact .= do_shortcode('[contact-form-7 id="13" title="Kontaktformulär 1" ""]');
	$contact .= '</div>';
	$contact .= "</div>\n";

	$contact .= "<div class='clear'></div>\n";


	// Autobots assemble!
	$panels .= "<div class='tab-pane {$active}' id='{$facility->getSlug()}'>";
	$panels .= "<!-- Map and staff contact info -->";
	$panels .= "<div class='col-xs-12 col-md-8 subContent'>{$mapstaff}</div>";

	$panels .= "<!-- Contact details & form -->";
	$panels .= "<div class='col-xs-12 col-md-4 subBorderLeft'>{$contact}</div>";
	$panels .= "</div>";

	//Script part for Lat/Long on first facility
	$jscript .= $active != '' ? "var mapLat = ".$address->getLat()."; var mapLong = ".$address->getLong()."; " : "";
	$active = '';
}
?>


<div class="wrap container mainText subpage" role="document">
	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<?php get_template_part('templates/page', 'header'); ?>
		</div>

		<script type="text/javascript">
			var responsiveMap;
			var beachMarker;
			var mapOptions;
			<?php echo $jscript; ?>
		</script>

		<ul class="nav nav-tabs mapTabs"><?php echo $tabs; ?></ul>

		<div class="tab-content"><?php echo $panels; ?></div>
	</div>
</div>