<div class="wrap container mainText PodsBrands subpage" role="document">
    <div class="row relative">
        <div class="col-xs-12 subHeader">
            <div class="page-header">
                <h1>Sidan hittades ej</h1>
            </div>
        </div>
    </div>

    <div class="row relative minPageHeight">
        <div class="col-md-8 subContent">
            Tyvärr kunde sidan ni efterfrågade ej hittas, navigera gärna vidare med hjälp av menyn.
        </div>

        <div class="col-md-4 subBorderLeft">
            <?php echo emitShowcases_vertical($cms); //exists in t1-lib.php ?>
        </div>
    </div>
</div>
