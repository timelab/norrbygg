<?php

// The standard header type.
// _________________________________________________________________________
// |                                                                        |
// |   	LOGO                                                    E-MAIL      |
// |   	LOGO 									                PHONE       |
// |________________________________________________________________________|
const HEADER_TYPE_1 = 'header-1';

// The standard footer type.
// __________________________________________________________________________
// |                                                                        |
// |                                                                        |
// |                      ADDRESS | PHONE | E-MAIL                          |
// |                                                                        |
// |________________________________________________________________________|
const FOOTER_TYPE_1 = 'footer-1';

// Footer with logo and spaced out content.
// _________________________________________________________________________
// |                 |                 |                  |                 |
// |                 |                 |                  |                 |
// |       LOGO      |     ADDRESS     |      E-MAIL      |      PHONE      |
// |                 |                 |                  |                 |
// |_________________|_________________|__________________|_________________|
const FOOTER_TYPE_2 = 'footer-2';

// Footer with text and spaced out content.
// _________________________________________________________________________
// |                                   |     Kontakt:     |                 |
// |                                   |     Adress 1     |     E-MAIL      |
// |              text                 |     Adress 2     |     TELEFON     |
// |                                   |     Adress 3     |     KARTA       |
// _________________________________________________________________________
// |                                                                        |
// |                                 LOGO                                   |
// |________________________________________________________________________|
const FOOTER_TYPE_3 = 'footer-3';

// Actual settings.
$t1config = array();

$t1config['fonts'] = array();
$t1config['fonts'][] = "https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700";
$t1config['fonts'][] = "https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700";


$t1config['header_type'] = HEADER_TYPE_1;
$t1config['footer_type'] = FOOTER_TYPE_3;

$t1config['showcases_enabled'] = true;
$t1config['brands_enabled'] = true;

// At which responsive level the brands should be hidden. Use bootstrap column
// names in a space separated list, e.g. "hidden-sm hidden-md". 
$t1config['hide_brands'] = '';


// SOCIAL NETWORKS INTEGRATION SETTINGS
//------------------------------------------------------------------------------
$t1config['social_integration_enabled'] = true;

// The link to the corresponding social networks. An empty string will be 
// interpreted as that network not being in use. 
$t1config['href_facebook'] = 'https://www.facebook.com/Norrbygg';
$t1config['href_instagram'] = '';
$t1config['href_instagram2'] = '';
$t1config['href_twitter'] = '';



// There are several types of facebook icons, each with their own distinct look and feel.
// Check the assets/img folder to look at them.
$t1config['facebook_icon'] = 'type_10';
//------------------------------------------------------------------------------
