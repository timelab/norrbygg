<?php

    $slug = pods_v( 'last', 'url' );
    //get pods object
    $mypod = pods( 'referensjobb', $slug );

?>
<!--[if IE 8]>
<style>
    .flex-direction-nav .flex-prev
    {
        left: -20px;
        height: 118px;
        background-image: url(<?php $site_url; ?>/assets/img/pilvansterIE.png);
    }
    .flex-direction-nav .flex-next
    {
        right: -21px!important;
        height: 118px;
        background-image: url(<?php $site_url; ?>/assets/img/pilhogerIE.png);
    }
</style>
<![endif]-->
<div class="wrap container mainText PodsBrands subpage" role="document">
    <div class="row relative">
        <div class="col-xs-12 subHeader">
            <div class="page-header">
                <h1><?php echo $mypod->field('title'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row relative minPageHeight">
        <div class="col-sx-12 subContent">

            <a href="referenser-nyproduktion-och-storre-renoveringar" class="tag <?php echo $referencestype == 'nyproduktion-och-storre-renoveringar' ? 'active' : ''?>">Nyproduktion och större renoveringar</a>
            <a href="referenser-badrum" class="tag <?php echo $referencestype == 'badrum' ? 'active' : ''?>">Badrum</a>
            <a href="referenser-kok" class="tag <?php echo $referencestype == 'kok' ? 'active' : ''?>">Kök</a>
            <a href="referenser-ovrigt" class="tag <?php echo $referencestype == 'ovrigt' ? 'active' : ''?>">Övrigt</a>

            <div class="row">
               <div class="col-xs-12">
                   <div id="slider" class="flexslider">
                       <ul class="slides">
                           <?php
                            //var_dump($mypod->field('bilder'));
                           foreach($mypod->field('bilder') as $image){
                               echo '<li><img src="' . pods_image_url($image['ID'],'reference-full') . '" alt="'. $image['post_title'] .'" /></li>';
                           }
                           ?>
                       </ul>
                   </div>
                   <div id="carousel" class="flexslider">
                       <ul class="slides">
                           <?php
                           //var_dump($mypod->field('bilder'));
                           foreach($mypod->field('bilder') as $image){
                               echo '<li><img src="' . pods_image_url($image['ID'],'reference-thumb') . '" alt="'. $image['post_title'] .'" /></li>';
                           }
                           ?>
                       </ul>
                   </div>

               </div>

            </div>
            <div class="row referense-info-border">
                <div class="col-xs-12 col-md-8 subContent">
                    <h2><?php echo $mypod->field('title'); ?></h2>
                    <?php echo $mypod->display('content'); ?>
                </div>
                <div class="col-xs-12 col-md-4 subBorderLeft">
                    <h2>Mer information</h2>
                    <?php echo $mypod->display('mer_info'); ?>
                    <div class="button-box"><a href="kontakta-oss" class="gradient">Till kontaktsidan</a></div>
                    <div class="button-box"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" class="gradient">Dela på Facebook</a></div>
                </div>
            </div>

        </div>
    </div>
</div>
