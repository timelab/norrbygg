<?php
global $cms;
global $t1config;
?>

<div class="row">
	<header class="banner navbar navbar-inverse" role="banner">

		<div class="navbar-header">
            <div class="minifacebook hidden-md hidden-lg hidden-sm"><a href="<?php echo $t1config['href_facebook']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_facebook_type_1.png" class="minifacebookimg"/></a></div>
            <div class="miniinstagram hidden-md hidden-lg hidden-sm"><a href="<?php echo $t1config['href_instagram2']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/instagram.png" class="miniinstagramimg"/></a></div>
	      	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        	<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	      	</button>
	      	<a class="navbar-brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
	    </div>
	
	    <nav class="collapse navbar-collapse" role="navigation">
			<?php
	        	if (has_nav_menu('primary_navigation'))
	        	{
	          		wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
	        	}
	      	?>
	    </nav>
	</header>
</div>
