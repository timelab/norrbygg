<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
	<script type="text/javascript">
		var baseUrl = '<?php echo get_template_directory_uri(); ?>';
	</script>
	<?php
		// All header info from WP and the template.
		wp_head();
	
		// Custom fonts from the config file.
		global $t1config;
		foreach ($t1config['fonts'] as $font) 
		{
			echo "<link href='{$font}' rel='stylesheet' type='text/css'>";
		}
	?>
	
	<link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	
	<?php
	    //Special include only on affected pages.
	    global $wp_query;
	    $toReplace = array(".php","page-templates/");
	    $template_name = str_replace($toReplace, "", get_post_meta($wp_query->post->ID, '_wp_page_template', true));

	    if (preg_match("#template-startsida(-nyheter)?#", $template_name) || $template_name == '')
		{
	        echo "<script src='" . get_template_directory_uri() . "/assets/js/plugins/jquery.flexslider.js'></script>\n";

	    }
	    
	    
	    // Map and form API scripts coontact page.
	    if($template_name == "template-kontakt")
		{
	        echo "<script src=\"https://maps.googleapis.com/maps/api/js?v=3.14&key=AIzaSyAjd7JOfF0INvvmNmcQDoNWP7I4F4vOTZw&sensor=false\"></script>\n";
	        echo "<script>
			mapLat = 65.586784;
			mapLong = 22.153072;
			var responsiveMap;
			var beachMarker;
		</script>\n";
	        echo "<script src='" . get_template_directory_uri() . "/assets/js/plugins/mapfunctions.js'></script>\n";
	        echo "<script src='" . get_template_directory_uri() . "/assets/js/plugins/formfunctions.js'></script>\n";
	    }
	?>
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->
</head>
