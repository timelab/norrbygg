<?php
	global $cms;
	global $t1config;
?>



<div class="row top-header">		
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 logo-mobile">
		<a href="/" target="_self"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logotype" class="topLogo"/></a>
        <div class="header-reseller">
            <div class="header-reseller-title">Officiell återförsäljare av:</div>
            <img src="<?= get_template_directory_uri(); ?>/assets/img/interkakel-logo.png">
        </div>
	</div>

	<div class="hidden-xs col-sm-6 col-md-5 col-lg-6">
        <?php
        // Social networking icons.
        //--------------------------------------------------------------------
        $social = emitSocial($cms);
        if (!empty($social))
        {
            echo "<div class='social'>$social</div>";
        }
        //--------------------------------------------------------------------

        // Contact info.
        //--------------------------------------------------------------------
        $contactInfo = '';
        $repArr = array ("(0)", "-", " "); // array för att fixa till telefonnr i länkar.

        // Build HTML representation.
        $facilities = getContactInfo($cms);

        if ($t1config['header_type'] === HEADER_TYPE_1)
        {
            $contactInfo .= "<div class='item'>";
            foreach ($facilities as $facility)
            {
                if (isset($facility['email']))
                {
                    $contactInfo .= "<div class='row topinfo-item'>";
                    $contactInfo .= "<div class='col-xs-12'>";
                    $contactInfo .= "<span class='glyphicon glyphicon-envelope'></span>";
                    $contactInfo .= "<a href='mailto:{$facility['email']}'>{$facility['email']}</a>";
                    $contactInfo .= "</div>";
                    $contactInfo .= "</div>";
                }
                if (isset($facility['telephone']))
                {
                    $contactInfo .= "<div class='row topinfo-item'>";
                    $contactInfo .= "<div class='col-xs-12'>";
                    $contactInfo .= "<span class='glyphicon glyphicon-phone'></span>";
                    $contactInfo .= "<a href='tel:" . str_replace($repArr, '', $facility['telephone']) . "'>{$facility['telephone']}</a>";
                    $contactInfo .= "</div>";
                    $contactInfo .= "</div>";
                }
            }
            $contactInfo .= "</div>";
        }

        if ($t1config['header_type'] === HEADER_TYPE_CUSTOM)
        {
            $contactInfo .= "<div class='item'>";

            $contactInfo .= "</div>";
        }


        echo "<div class='top-info hidden-xs'>$contactInfo</div>";
        //--------------------------------------------------------------------
        ?>

	</div>
</div>