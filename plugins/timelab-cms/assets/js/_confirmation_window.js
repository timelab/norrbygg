if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };
}


function confirmation_window(title, message, yes_function, no_function) {
    var modal = '' +
        '<div class="modal fade" id="confirmation_window" data-backdrop="static">' +
        '   <div class="modal-dialog">' +
        '       <div class="modal-content">' +
        '           <div class="modal-header">' +
        '               <h4 class="modal-title">{0}</h4>' +
        '           </div>' +
        '           <div class="modal-body">' +
        '               <p>{1}</p>' +
        '           </div>' +
        '           <div class="modal-footer">' +
        '               <div class="col-sm-4">' +
        '                   <a href="#" class="btn btn-block btn-lg btn-success confirm" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ja</a>' +
        '               </div>' +
        '               <div class="col-sm-8">' +
        '                   <a href="#" class="btn btn-block btn-lg btn-danger cancel" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Nej</a>' +
        '               </div>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>';


    $('body').prepend(modal.format(title, message));

    $('#confirmation_window .confirm').click(function () {
        if (yes_function) {
            yes_function();
        }
    });

    $('#confirmation_window .cancel').click(function () {
        if (no_function) {
            no_function();
        }
    });

    $('#confirmation_window').modal('show');
}

$(function () {
    $('a[data-confirm="true"]').click(function (e) {
        var href = $(this).attr('href');
        confirmation_window($(this).data('confirm-title'), $(this).data('confirm-text'), function() {
            window.location.href = href;
        });

        e.preventDefault();
    });
});