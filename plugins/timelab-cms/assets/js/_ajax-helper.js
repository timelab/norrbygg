$(function () {
    $('form a[data-ajax_url]').click(function () {
        var url = $(this).data('ajax_url'),
            $form = $(this).closest('form').first();

        $.post(url, $form.serialize(), function (data, status) {
            if (status === 'success') {
                // Set original-value to current value after save
                $('form').find('textarea, input, select').each(function () {
                    $(this).data('original-value', $(this).val());
                });

                $('form .dirty').removeClass('dirty');

                bindDirtyChecker();

                //toastr.success('Datan har sparats.');

                location.reload();
            } else {
                toastr.error('Datan kunde inte sparas!');
            }
        });
    });
});