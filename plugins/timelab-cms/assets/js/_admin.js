if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };
}

function create_media_frame(title, button_text, media_function, multiple) {
    var frame = wp.media.frames.customHeader = wp.media({
        title: title,
        library: {
            type: 'image'
        },
        button: {
            text: button_text
        },
        multiple: multiple
    });

    frame.on('select', function() {
        var attachment = frame.state().get('selection').toJSON();

        for (var i = 0; i < attachment.length; i++) {
            media_function(attachment[i]);
        }
    });

    return frame;
}

$(function () {

    // Stoppa "Är du säker på att du vill lämna sidan" varning då man sparar
    $('#save').click(function (e) {
        e.preventDefault();
        $('#publish').trigger('click');
    });

    $('select[data-value]').each(function () {
        $(this).find("option[value='" + $(this).data('value') + "']").prop('selected', true);
    });

    $('[data-toggle="tooltip"]').tooltip();

    // Panelinställningar
    $('a[href="#screen-options-wrap"]').click(function () {
        $('#screen-options-wrap').removeClass('hidden');
    });

    // Timelab CMS inställnigar skript
    var install_input = $('input[name="install_timelab_cms"]'),
        install_button = $('#install-timelab-cms-button');

    if (parseFloat(install_input.val()) === 1) {
        install_button.addClass('disabled');
        install_button.text('Timelab CMS redan installerad');
    }

    install_button.click(function () {
        install_input.val(1);
        $('form').submit();
    });
});