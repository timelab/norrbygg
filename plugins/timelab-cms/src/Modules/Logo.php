<?php

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;
use Timelab\Cms\ModuleAbstract;

require_once('LogoApi.php');

/**
 * Class Logo
 * @package Timelab\Cms\Modules
 */
class Logo extends ModuleAbstract {

    private $api;

    public function __construct()
    {
        parent::__construct();
        $this->api = new LogoApi();
    }


    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return false;
    }

    public function addToMenu()
    {
        return null;
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        // TODO: Implement jsFiles() method.
    }

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {
        // TODO: Implement onInitialize() method.
    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {
        // TODO: Implement getDependencies() method.
    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        // TODO: Implement getMenuOrder() method.
    }

    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        // TODO: Implement earlyExecute() method.
    }

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        // TODO: Implement execute() method.
    }

    /**
     * Returns the API interface of the module
     * @return ApiInterface
     */
    public function getApi()
    {
        return $this->api;
    }
} 