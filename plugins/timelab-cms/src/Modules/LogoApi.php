<?php

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;

class LogoApi implements ApiInterface {

    /**
     * Gets the Logo for the website
     * @return null|string The url of the image, `null` if not set.
     */
    public function getLogo() {

        if (get_option('customer_logo') === null) {
            return null;
        }

        $customerUrl = get_option('customer_logo');

        return trim($customerUrl, '/');
    }

} 