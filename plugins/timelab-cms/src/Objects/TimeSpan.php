<?php
/**
 * Created by PhpStorm.
 * User: patrik
 * Date: 9/17/14
 * Time: 3:58 PM
 */

namespace Timelab\Cms\Objects;


use Timelab\Cms\DatabaseObjectAbstract;

class TimeSpan extends DatabaseObjectAbstract {

    private $to = null;
    private $from = null;
    private $department = null;

    /**
     * The post type of the object in the database, used internally by the object when saving.
     * Note: Post type can only be a maximum of 20 characters!
     * @return string The post type of the object
     */
    public function getPostType()
    {
        return "timelab_cms_timespan";
    }

    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    public function canUserSave()
    {
        return true;
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    public function validateSave()
    {
        if ($this->getTitle() == ('' || null) || $this->getParentId() == ('' || null)) {
            return false;
        }

        return true;
    }

    /**
     * Runs after the loadFromPost method, used to get all custom_fields and other misc data from the database and apply
     * to the object.
     */
    protected function loadFromPostFields()
    {
        $this->setFrom(get_post_meta($this->getId(), 'from', true));
        $this->setTo(get_post_meta($this->getId(), 'to', true));
        $this->setDepartment(get_post_meta($this->getId(), 'department', true));
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    protected function saveFields()
    {
        update_post_meta($this->getId(), 'from', $this->getFrom());
        update_post_meta($this->getId(), 'to', $this->getTo());
        update_post_meta($this->getId(), 'department', $this->getDepartment());
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string The department that the TimeSpan belongs to
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * Returns the TimeSpan as an array.
     * @return array The timespan
     */
    public function asArray() {
        return array(
            'id'            => $this->getId(),
            'order'         => $this->getOrder(),
            'title'         => $this->getTitle(),
            'from'          => $this->getFrom(),
            'to'            => $this->getTo(),
            'department'    => $this->getDepartment()
        );
    }

} 