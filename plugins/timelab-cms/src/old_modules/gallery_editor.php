<?php

class gallery_editor extends cms_module {

    public function __construct(&$cms) {
        add_action('admin_init', array($this, 'create_post_type'));
        add_action('admin_menu', array($this, 'create_gallery_page'));
        add_action('admin_init', array($this, 'add_category_to_attachments'));
    }

    function add_category_to_attachments() {
        register_taxonomy_for_object_type('category', 'attachment');
    }

    function create_post_type() {
        $post_type_args =
            array(
                'labels' => array(
                    'name' => 'Bildtagg'
                ),
                'description' => 'Detta är en tagg för en galleribild',
                'public' => true,
                'exclude_from_search' => true,
                'supports' => array('title')
            );

        register_post_type('timelab_cms_tag', $post_type_args);

        wp_create_category("Galleri");
    }

    function create_gallery_page() {
        $page_function = 'output_gallery_editor';

        if (isset($_GET['page']) && $_GET['page'] == 'timelab_cms_gallery' && isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'save':
                    $this->save_gallery();
                    break;
            }
        }

        add_menu_page('Galleri', 'Galleri', 'edit_pages', 'timelab_cms_gallery', array($this, $page_function), 'dashicons-camera', '45.12565');
    }

    function output_gallery_editor() {
        $this->admin_page_template_top();

        echo '
            <div class="col-sm-9">
                <div id="gallery-image-modal" class="modal fade" data-backdrop="static">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-body form-horizontal">
                        <div class="col-sm-12">
                            <div class="col-sm-3 col-sm-offset-4" id="gallery-image-preview">

                            </div>
                            <input type="hidden" name="changed" value="false" />
                            <input type="hidden" name="id" value="" />
                            <div class="clear"></div>
                        </div>
                        <hr />
                        <div class="col-sm-12">
                            <div class="form-group">

                                <label for="title" class="col-sm-1 control-label">Titel</label>
                                <div class="col-sm-11">
                                    <input type="text" class="form-control" name="title"></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="text" class="col-sm-1 control-label">Text</label>
                                <div class="col-sm-11">
                                    <textarea class="form-control" name="text"></textarea>
                                </div>';


        $tags = $this->get_tags();

        if (!empty($tags)) {

            echo '
            <div class="clear"></div>
            <hr />
            <h3>Taggar</h3>
            <ul class="list-inline tag-list">';
            foreach ($tags as $name=>$tag) {
                echo '<li><input type="checkbox" name="tags[]" value="' . $tag . '" />' . $name . '</li>';
            }
            echo "</ul>";
        }



        echo '                  </div>
                        </div>
                        <div class="clear"></div>
                      </div>
                      <div class="modal-footer">
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-block btn-danger" id="delete-image" data-dismiss="modal">Ta bort</button>
                            </div>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-block btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>';

        echo '<div class="row">';

        $posts = $this->get_images();

        // Skapa template bild (Används av javascriptet för att skapa nya bilder)
        echo '<div class="hidden" id="gallery-template">';
        $this->output_image(null);
        echo '</div>';

        $action_url = admin_url('admin.php?page=timelab_cms_gallery&action=save');
        echo "<form action='$action_url' method='post'>";
        echo '<div id="changed-images" class="hidden"></div><div id="removed-tags" class="hidden"></div><div id="removed-images" class="hidden"></div>';
        echo '<div class="col-sm-9" id="gallery-list">';


        foreach($posts as $post) {
            $this->output_image($post);
        }

        echo '<div class="clear"></div>';
        echo '</div>';

        echo '
            <div class="col-sm-3 sidemenu">
                <button class="btn btn-success btn-lg btn-block" target="submit"><span class="glyphicon glyphicon-check"></span> Spara ändringar</button><br />
                <input type="submit" class="hidden"></input>
                <div class="btn btn-success btn-block" id="upload-image">Ladda upp bilder</div>';

        // Hämta taggar
        $this->output_tag_editor();

        echo '</div>';

        echo '</div>'; // End ROW

        echo '</form>';

        $this->admin_page_template_bottom();
    }

    /**
     * @param $image array Bilden som ska skrivas ut
     */
    function output_image($image){
        if (!isset($image->ID)) {
            echo '<div class="col-sm-3 col-xs-6 thumbnail-image no-tags" data-id="-1"
            data-tags="" data-title="" data-text="">
                <img src="" />
            </div>';
            return;
        }

        $classes = " ";
        if (empty($image->post_excerpt)) {
            $classes .= "no-tags";
        }

        echo '<div class="col-sm-3 col-xs-6 thumbnail-image' . $classes . '" data-id="' . $image->ID . '"
            data-tags="' . $image->post_excerpt . '" data-title="' . $image->post_title . '" data-text="' . $image->post_content . '">
                <img src="' . wp_get_attachment_image_src($image->ID, 'medium', false)[0] . '" />
            </div>';
    }

    function get_images() {
        $posts = get_posts(array(
            'post_type' => 'attachment',
            'category'  => get_term_by('name', 'Galleri', 'category')->term_id,
            'numberposts' => -1,
            'post_mime_type' =>'image',
            'post_status' => null,
            'post_parent' => null, // any parent
        ));

        foreach ($posts as $post) {
            $tags = explode('#', trim($post->post_excerpt));
            unset($tags[0]);

            foreach ($tags as $index=>$tag) {
                $tags[$index] = trim($tag);
            }

            $post->tags = $tags;
        }

        return $posts;
    }

    function get_tags() {
        $posts = get_posts(array(
            'post_type' => 'timelab_cms_tag',
            'numberposts' => -1,
            'order'=> 'ASC',
            'orderby' => 'title',
            'post_status' => null,
            'post_parent' => null
        ));

        $tags = array();

        foreach ($posts as $post) {
            $tags[$post->post_title] = $post->post_name;
        }

        return $tags;
    }

    function save_gallery() {
        // Spara taggar
        if (!empty($_POST['new_tag'])) {
            foreach ($_POST['new_tag'] as $tag) {
                wp_insert_post(array(
                    'post_title' => $tag,
                    'post_status' => 'publish',
                    'post_type' => 'timelab_cms_tag'
                ));
            }
        }

        // Ta bort taggar
        if (!empty($_POST['removed_tag_name'])) {
            global $wpdb;

            foreach ($_POST['removed_tag_name'] as $tag) {


                $prepared_tag_name = esc_sql($tag);
                $tag_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name ='".$prepared_tag_name."' AND post_type='timelab_cms_tag'");

                wp_delete_post($tag_id);
            }
        }

        // Uppdatera bilder
        if (!empty($_POST['image_id'])) {
            foreach ($_POST['image_id'] as $index=>$id) {
                $post_args = array(
                    'ID'            => $_POST['image_id'][$index],
                    'post_title'    => $_POST['image_title'][$index],
                    'post_excerpt'  => $_POST['image_tags'][$index],
                    'post_content'  => $_POST['image_text'][$index]
                );

                $category = get_term_by('name', 'Galleri', 'category');

                if ($category == false) {
                    $category_id = wp_create_category('Galleri');
                } else {
                    $category_id = $category->term_id;
                }


                wp_set_post_categories($id, array( $category_id ));

                if (get_post_type($id) == "attachment") {
                    wp_update_post( $post_args );
                }

            }
        }

        // Ta bort bilder
        if (!empty($_POST['removed_image_id'])) {
            foreach ($_POST['removed_image_id'] as $index=>$id) {
                if (get_post_type($id) == "attachment") {
                    wp_delete_post( $id );
                }
            }
        }

        $this->clear_cache();
        wp_redirect(admin_url('admin.php?page=timelab_cms_gallery&message=save_success'));
    }

    public function output_tag_editor()
    {
        $tags = $this->get_tags();

        echo '<hr /><h3>Taggar</h3>';

        echo '<div class="input-group">
                <input type="text" id="new-tag-input" class="form-control"/>
                <span class="input-group-btn">
                    <button type="button" class="btn btn-success" id="add-tag-button">Lägg till</span>
                </span>
              </div>';

        echo '<ul class="tag-admin-list">';
        foreach ($tags as $name=>$tag) {
            echo '<li data-tag-name=' . $tag . '>
                <div class="input-group">
                    <input type="text" class="form-control" readonly value="' . $name . ' (' . $tag . ')"></input>
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-danger remove-tag">Ta bort</button>
                    </span>
                </div>
            </li>';
        }
        echo '</ul>';
    }
    
    /**
     * Hämtar samtliga bild med specificerad tagg
     */
    public function get_images_by_tag($tag) {
        $data = $this->get_images();
        
        $images = array();
        
        foreach ($data as $image) {
            if (in_array($tag, $image->tags)) {
                $images[] = $image;
            }
        }
        
        return $images;
    }


}
