<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 2013-11-20
 * Time: 18:11
 */

/**
 * Class contact_detail
 * En kontaktdetalj är allt från ett telefonnummer till en epost address. Denna innehåller en typ och ett värde
 */
class contact_detail {

    /**
     * @var int Den unika identifiern för typen.
     */
    public $ID;

    /**
     * @var int Ordningen den dyker upp i. 0 är först.
     */
    public $order;

    /**
     * @var string Typen för kontaktdetaljen. Ex "email"
     */
    public $type;

    /**
     * @var string Värdet för kontaktdetaljen. Ex "patrik@timelab.se"
     */
    public $value;

    /**
     * @var string Namnet som ska anges för kontaktdetaljen. Om värdet är NULL så returnerar den default värdet för typen.
     */
    private $name;

    /**
     * @param $ID int Den unika identifiern för typen.
     * @param $order int Ordningen den dyker upp i. 0 är först.
     * @param $type string Typen för kontaktdetaljen. Ex "email"
     * @param $value string Värdet för kontaktdetaljen. Ex "patrik@timelab.se"
     * @param null $name string Namnet som ska anges för kontaktdetaljen. Om värdet är NULL så returnerar den default värdet för typen.
     */
    function __construct($ID, $order, $type, $value, $name = null) {
        $this->ID = $ID;
        $this->order = $order;
        $this->type = $type;
        $this->value = $value;
        $this->name = $name;
    }

    /**
     * Hämtar ut namnet på kontaktdetaljen. Om inget namn är satt så försöker den hämta namnet på typen (Tex typen 'email' returnerar 'E-post')
     * @return string Namnet på kontaktdetaljen. Ex "E-post"
     */
    public function get_name() {

        // Om namnet är satt, hämta ut detta
        if (!empty($this->name)) {
            return $this->name;
        }

        // Hämta namnet accosierat med typen av kontaktdetaljen
        switch ($this->type) {
            case 'telephone':
                $name = 'Telefon';
                break;
            case 'mobile':
                $name = 'Mobil';
                break;
            case 'email':
                $name = 'E-post';
                break;
            default:
                // Varna ifall fältet inte har något formatterat namn
                trigger_error("'{$this->type}' type has no name", E_USER_WARNING);
                $name = $this->type;
                break;
        }

        return $name;
    }

    /**
     * Hämtar ut det formatterade värdet. Den försöker formattera värdet baserat på typen. (Tex typen 'email' returnerar "<a href='mailto:{$varde}'>{$varde}</a>") annars hämtar den bara ut värdet
     * @return string Formatterade värdet för kontaktdetaljen. Ex ("<a href='mailto:patrik@timelab.se'>patrik@timelab.se</a>")
     */
    public function get_formatted_value() {

        $value = $this->value;

        switch ($this->type) {
            case 'email':
                $value = "<a href='mailto:{$value}'>{$value}</a>";
                break;
            case 'mobile':
            case 'telephone':
                $value = "<a href='tel:{$value}'>{$value}</a>";
                break;
        }

        return $value;
    }
} 