<?php

require_once('timespan.php');
require_once('address.php');
require_once('person.php');

/**
 * Class facility
 * En anläggning innehåller samtlig information om anläggningen. Allt från öppettider till personal.
 */
class facility {

    /**
     * @var int Unika identifiern för anläggningen
     */
    public $ID;

    /**
     * @var string Namnet på anläggningen.
     */
    public $name;

    /**
     * @var string Slugen, ett url vänligt namn för anläggningen
     */
    public $slug;

    /**
     * @var int Ordningen anläggningen är i. 0 är först.
     */
    public $order;

    /**
     * Skapar en ny anläggning
     * @param $ID int Anläggningens ID
     * @param $name string Namnet på anläggningen
     * @param $slug string Anläggningens slug (URL vänliga versionen av namnet)
     * @param $order int Vilken ordning anläggningen dyker upp i listan
     */
    public function __construct($ID, $name, $slug, $order) {
        $this->ID   = $ID;
        $this->name = $name;
        $this->slug = $slug;
        $this->order = $order;
    }

    /**
     * Hämtar ut anläggningens öppettider
     * @return timespan[] Array med samtliga tidsspan
     */
    public function get_opening_hours() {
        $posts = get_posts(array(
            'post_type' => 'timelab_cms_timespan',
            'posts_per_page' => -1,
            'post_parent' => $this->ID,
            'meta_key' => 'order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC'
        ));

        $opening_hours = array();

        foreach ($posts as $post) {

            $timespan = new timespan(
                $post->ID,
                $post->post_title,
                get_post_meta($post->ID, 'from', true),
                get_post_meta($post->ID, 'to', true),
                get_post_meta($post->ID, 'order', true)
            );

            $opening_hours[] = $timespan;
        }

        return $opening_hours;
    }

    /**
     * Hämtar ut anläggningens samtliga gatuaddresser
     * @return address Den primära adressen
     */
    public function get_address() {
        $posts = get_posts(array(
            'post_type' => 'timelab_cms_address',
            'posts_per_page' => 1,
            'post_parent' => $this->ID,
            'meta_key' => 'order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC'
        ));

        $addresses = array();

        foreach ($posts as $post) {
            $address = new address(
                $post->ID,
                $post->post_title,
                get_post_meta($post->ID, 'formatted', true),
                get_post_meta($post->ID, 'street', true),
                get_post_meta($post->ID, 'zip', true),
                get_post_meta($post->ID, 'city', true),
                get_post_meta($post->ID, 'lat', true),
                get_post_meta($post->ID, 'long', true),
                get_post_meta($post->ID, 'order', true)
            );

            $addresses[] = $address;
        }

        // Kommer eventuellt returnera fler än 1 address.
        if (isset($addresses[0])) {
            return $addresses[0];
        }
    }

    /**
     * Hämtar ut all personal för anläggningen.
     * @return person[] Array med samtlig personal för anläggningen
     */
    public function get_personnel() {
        $posts = get_posts(array(
            'post_type' => 'timelab_cms_person',
            'posts_per_page' => -1,
            'post_parent' => $this->ID,
            'meta_key' => 'order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC'
        ));

        $personnel = array();

        //$order = get_post_meta($post->ID, 'order', true);

        foreach ($posts as $index=>$post) {
            $person = new person(
                $post->ID,
                $index,
                $post->post_title,
                get_post_meta($post->ID, 'role', true),
                get_post_meta($post->ID, 'department', true),
                get_post_meta($post->ID, 'description', true),
                get_post_meta($post->ID, 'media_id', true)
            );

            $personnel[] = $person;
        }

        return $personnel;
    }

    public function get_contact_details() {
        $posts = get_posts(array(
            'post_type'     => 'timelab_cms_contact',
            'post_parent'   => $this->ID,
            'meta_key'      => 'order',
            'orderby'       => 'meta_value_num',
            'order'         => 'ASC'
        ));

        $contact_details = array();

        foreach ($posts as $post) {
            $contact_detail = new contact_detail(
                $post->ID,
                get_post_meta($post->ID, 'order', true),
                get_post_meta($post->ID, 'type', true),
                get_post_meta($post->ID, 'value', true),
                get_post_meta($post->ID, 'name', true)
            );

            $contact_details[] = $contact_detail;
        }

        return $contact_details;
    }

    public function get_social_details() {
        $posts = get_posts(array(
            'post_type'     => 'timelab_cms_contact',
            'post_parent'   => $this->ID,
            'meta_key'      => 'order',
            'orderby'       => 'meta_value_num',
            'order'         => 'ASC'
        ));

        $social_details = array();

        foreach ($posts as $post) {
            $social_detail = new social_detail(
                $post->ID,
                get_post_meta($post->ID, 'order', true),
                get_post_meta($post->ID, 'type', true),
                get_post_meta($post->ID, 'value', true),
                get_post_meta($post->ID, 'name', true)
            );

            $social_details[] = $social_detail;
        }

        return $social_details;
    }
} 