# Args
ARG WP_VERSION=5.8.3
ARG WP_LANG=sv_SE

#----------
#
# Utility
#
#----------

#
# Composer
#
FROM composer:2 AS composer-builder
WORKDIR /app
COPY ./composer.json /app/composer.json
COPY ./composer.lock /app/composer.lock
COPY ./auth.json /app/auth.json
COPY ./plugins /app/plugins
RUN composer install

#----------
#
# Application
#
#----------

#
# Base
#
FROM wordpress:${WP_VERSION}-fpm AS base
WORKDIR /var/www/html

# install the PHP extensions we need
RUN mkdir -p /usr/src/php/ext
RUN apt-get update && apt-get install -y zlib1g-dev libpng-dev libonig-dev
RUN pecl install memcache-4.0.5.2 && docker-php-ext-enable memcache
RUN docker-php-ext-configure gd && docker-php-ext-install gd
RUN docker-php-ext-configure opcache && docker-php-ext-install opcache
RUN docker-php-ext-configure mbstring && docker-php-ext-install mbstring
RUN docker-php-ext-install mysqli

#
# Build assets
#
FROM debian AS asset-builder

SHELL [ "/bin/bash", "-l", "-c" ]

RUN apt-get update && apt-get install -y curl
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

RUN nvm install --lts \
    && nvm alias default lts/* \
    && nvm use default 

COPY ./themes /site-themes

WORKDIR /site-themes/timelab-t1
RUN nvm install && nvm use && npm install -g yarn && npm install -g grunt-cli && yarn && grunt

#
# Prod
#
FROM base AS prod
EXPOSE 80

# Copy files
COPY --from=wordpress:cli /usr/local/bin/wp /usr/local/bin/wp
COPY --from=composer-builder /app/plugins /var/www/html/wp-content/plugins
COPY ./scripts/entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint.sh
COPY ./themes /var/www/html/wp-content/themes
COPY --from=asset-builder /site-themes/timelab-t1/assets/css /var/www/html/wp-content/themes/timelab-t1/assets/css
COPY --from=asset-builder /site-themes/timelab-t1/assets/js/scripts.min.js /var/www/html/wp-content/themes/timelab-t1/assets/js/scripts.min.js
COPY ./config/uploads.ini /usr/local/etc/php/conf.d/uploads.ini
COPY ./config/w3tc-config /var/www/html/wp-content/w3tc-config
RUN chmod -R a-w /var/www/html/wp-content/w3tc-config
COPY ./config/htaccess /var/www/html/.htaccess
RUN chmod a-w /var/www/html/.htaccess
RUN chown -R www-data:www-data /var/www/html/wp-content/
COPY ./config/w3tc/* /var/www/html/wp-content/

ENTRYPOINT ["entrypoint.sh"]
CMD ["php-fpm"]

#
# Dev
#
FROM base AS dev
EXPOSE 80
RUN mkdir -p /var/www/html/wp-content/uploads && chown -R www-data:www-data /var/www/html/wp-content/uploads
CMD ["php-fpm"]

#----------
#
# Nginx
#
#----------

FROM wordpress:${WP_VERSION}-fpm AS wp
RUN chown -R root:root /usr/src/wordpress


#
# Nginx Base
#
FROM nginx:1 AS nginx-base
COPY --from=wp /usr/src/wordpress /var/www/html
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

#
# Nginx Dev
#
FROM nginx-base AS nginx-dev

#
# Nginx Prod
#
FROM nginx-base AS nginx-prod
COPY --from=composer-builder /app/plugins /var/www/html/wp-content/plugins
COPY ./themes /var/www/html/wp-content/themes
COPY --from=asset-builder /site-themes/timelab-t1/assets/css /var/www/html/wp-content/themes/timelab-t1/assets/css
COPY --from=asset-builder /site-themes/timelab-t1/assets/js/scripts.min.js /var/www/html/wp-content/themes/timelab-t1/assets/js/scripts.min.js

